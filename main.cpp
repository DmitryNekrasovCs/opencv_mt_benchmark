#include <iostream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

static const std::string g_Path = "c:\\Users\\nekrasov\\Downloads\\van_Gogh.jpg";

class ParallelProcess : public cv::ParallelLoopBody
{
public:
    ParallelProcess(const cv::Mat& _input_image, cv::Mat& _out_image, int _size, int _diff)
        : m_ImageIn(_input_image)
        , m_ImageOut(_out_image)
        , m_Size(_size)
        , m_Diff(_diff)
    {}

    virtual void operator()(const cv::Range& _range) const override {
        for (int i = _range.start; i < _range.end; i++) {
            cv::Mat in(m_ImageIn, cv::Rect(0, (m_ImageIn.rows / m_Diff) * i,
                m_ImageIn.cols, m_ImageIn.rows / m_Diff));
            cv::Mat out(m_ImageOut, cv::Rect(0, (m_ImageOut.rows / m_Diff) * i,
                m_ImageOut.cols, m_ImageOut.rows / m_Diff));
            cv::GaussianBlur(in, out, cv::Size(m_Size, m_Size), 0);
        }
    }

private:
    cv::Mat m_ImageIn;
    cv::Mat& m_ImageOut;
    int m_Size;
    int m_Diff;
};

static const int g_IterNum = 3;
static const int g_GaussKernelSize = 1001;

int main() {

    auto image_in = cv::imread(g_Path);
    cv::Mat image_out = cv::Mat::zeros(image_in.size(), CV_8UC3);

    std::cout << "My parallel_for_:" << std::endl;
    for (int i = 0, thread_num = 1; i < g_IterNum; i++, thread_num *= 2) {
        auto start_time = cv::getTickCount();
        cv::parallel_for_(cv::Range(0, thread_num), ParallelProcess(image_in, image_out, g_GaussKernelSize, thread_num));
        auto full_time = (cv::getTickCount() - start_time) / cv::getTickFrequency();
        std::cout << "-Thread num: " << thread_num << ", Full time: " << full_time << std::endl;
    }

    std::cout << "\nOpenCV setNumThreads:" << std::endl;
    for (int i = 0, thread_num = 1; i < g_IterNum; i++, thread_num *= 2) {
        cv::setNumThreads(thread_num);
        auto start_time = cv::getTickCount();
        cv::GaussianBlur(image_in, image_out, cv::Size(g_GaussKernelSize, g_GaussKernelSize), 0);
        auto full_time = (cv::getTickCount() - start_time) / cv::getTickFrequency();
        std::cout << "-Thread num: " << cv::getNumThreads() << ", Full time: " << full_time << std::endl;
    }

    cv::imshow("image", image_in);
    cv::imshow("blur", image_out);

    cv::waitKey(0);

    return 0;
}
