QT += core
QT -= gui

CONFIG += c++14

TARGET = opencv_mt_benchmark
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

INCLUDEPATH += c:/lazy/opencv/build/install/include

LIBS += -L"c:/lazy/opencv/build/install/x86/mingw/lib/"
LIBS += \
        -lopencv_core2413 \
        -lopencv_highgui2413 \
        -lopencv_imgproc2413
